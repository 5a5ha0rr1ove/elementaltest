using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System;
using Elementual;

public class PlayerHands : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private Hand leftHand;
    [SerializeField] private Hand rightHand;
    #endregion

    #region Consts
    private static Vector3 InHandPosition { get { return new Vector3(0, 0, 0); } }
    private const string weaponTagConst = "Weapon";
    #endregion

    #region Unity methods
    private void Awake()
    {
        CheckHands();
    }
    /// <summary>
    /// Called on start. Checks child objects for weapons. Make debug easier;
    /// </summary>
    private void CheckHands()
    {
        leftHand.CheckHand();
        rightHand.CheckHand();
    }
    #endregion

    #region public methods that intup handler uses
    public void PickUpOrDropLeft(GameObject rifleScope)
    {
        PickUpOrDrop(leftHand, rifleScope);
    }
    public void PickUpOrDropRight(GameObject rifleScope)
    {
        PickUpOrDrop(rightHand, rifleScope);
    }
    
    public void TryShootLeft(GameObject rifleScope)
    {
        PullTheTrigger(leftHand, rifleScope);
    }
    public void TryShootRight(GameObject rifleScope)
    {
        PullTheTrigger(rightHand, rifleScope);
    }
    public void ReleaseLeft()
    {
        ReleaseTheTrigger(leftHand);
    }
    public void ReleaseRight()
    {
        ReleaseTheTrigger(rightHand);
    }
    #endregion

    #region Common methods for working with the hands
    private void PickUpOrDrop(Hand hand, GameObject rifleScope)
    {
        if (!hand.emptyHand)
        {
            hand.DropWeapon();
        }
        else
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(rifleScope.transform.position);
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.gameObject.tag == weaponTagConst)
                {
                    hand.PickUpWeapon(hit.collider.gameObject);
                }
            }
        }
    }

    private bool PullTheTrigger(Hand hand, GameObject rifleScope)
    {
        if (!hand.emptyHand)
        {
            hand.PullTheTrigger(rifleScope);
        }
        return !hand.emptyHand;
    }

    private void ReleaseTheTrigger(Hand hand)
    {
        if (!hand.emptyHand)
        {
            hand.ReleaseTheTrigger();
        }
    }
    #endregion

    #region Local hand class grouping logic and variables for simplified work
    [Serializable] private class Hand
    {
        [SerializeField] public GameObject hand;
        public bool emptyHand { get { return emptyhand_; } private set { emptyhand_ = value; } }
        private bool emptyhand_ = true;
        private GameObject handWeapon;
        private IHandWeapon handWeaponInterface;

        public void DropWeapon()
        {
            emptyHand = true;
            handWeapon.transform.position = new Vector3(hand.transform.position.x, hand.transform.position.y - 0.5f, hand.transform.position.z);
            handWeapon.transform.parent = null;
            handWeaponInterface.ReleaseTheTrigger();
            handWeaponInterface.SetWorld();
            handWeaponInterface = null;
            handWeapon = null;
        }

        public void PickUpWeapon(GameObject weapon)
        {
            handWeaponInterface = weapon.GetComponent<IHandWeapon>();
            if (handWeaponInterface != null)
            {
                emptyHand = false;
                handWeapon = weapon;
                weapon.transform.parent = hand.transform;
                weapon.transform.localPosition = InHandPosition;
                handWeaponInterface.SetHand();
                handWeaponInterface.SetHandRotation();
            }
            else
            {
                Debug.Log("Cnnot get component IHandWeapon from gameObject");
            }
        }

        public void PullTheTrigger(GameObject rifleScope)
        {
            handWeaponInterface.PullTheTrigger(rifleScope);
        }

        public void ReleaseTheTrigger()
        {
            handWeaponInterface.ReleaseTheTrigger();
        }

        public void CheckHand()
        {
            if (hand.transform.childCount > 0)
            {
                HandWeapon someWeapon = hand.GetComponentInChildren<HandWeapon>();
                if (someWeapon != null)
                {
                    SetWeapon(someWeapon.gameObject, someWeapon);
                }
            }
        }

        private void SetWeapon(GameObject weapon, HandWeapon handWeaponInterface_)
        {
            emptyHand = false;
            handWeapon = weapon;
            handWeaponInterface = handWeaponInterface_;
            handWeaponInterface.SetHand();
        }
    }
    #endregion
}
