using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceSlider : MonoBehaviour
{

    #region Serialized Field
    [SerializeField] LayerMask groundMask;
    #endregion

    #region work field
    private Vector3 normal;
    #endregion

    #region Unity method
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == groundMask)
        {
            normal = collision.contacts[0].normal;
        }
    }
    #endregion

    public Vector3 Project(Vector3 forward)
    {
        return forward - Vector3.Dot(forward, normal) * normal;
    }

}
