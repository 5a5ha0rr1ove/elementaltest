using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;

public class InputHandler : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private Transform playerBody; // Reference to the player's body (usually the root object of the player).
    [SerializeField] private Transform camera_; // Reference to the main camera.
    [SerializeField] private PlayerHands playerHands; // Reference to the PlayerController script.
    [SerializeField] private PhysicsMovement movementPhysyc; // Reference to the PhysicsMovement script.
    [SerializeField] private GameObject rifleScope; // Reference to the rifle scope for aiming.
    [Space][SerializeField] private EnemyController target;

    [Space][SerializeField] private int sensivityVertical = 10; // Vertical sensitivity for mouse input.
    [SerializeField] private int sensivityHorizontal = 20; // Horizontal sensitivity for mouse input.
    [SerializeField] private float horizontalSpeed = 10; // Speed of horizontal movement.
    #endregion

    #region work fields
    private bool horizontalMovePerformed = false; // Flag to track whether horizontal movement is being performed.
    private Vector3 horizontalVelocity = Vector3.zero; // The horizontal velocity calculated from the input.
    private Vector2 _horizontalInput = Vector3.zero; // The raw horizontal input from the player.
    #endregion

    #region Unity methods
    private void Start()
    {
        LockUnlockMouse();
    }

    private void Update()
    {
        if (horizontalMovePerformed)
        {
            HorizontalMove();
        }
    }

    // Perform horizontal movement.
    private void HorizontalMove()
    {
        Vector3 horizontalVelocity = (playerBody.transform.right * _horizontalInput.x + playerBody.transform.forward * _horizontalInput.y) * horizontalSpeed;
        movementPhysyc.Move(horizontalVelocity);
    }
    #endregion

    #region Handle events from input system

    public void MouseLeftHandler(InputAction.CallbackContext context)
    {
        float mouseMoveDelta = context.valueSizeInBytes * sensivityHorizontal * Time.deltaTime;
        Vector3 res2 = Vector3.up * mouseMoveDelta;
        playerBody.Rotate(new Vector3(0, -1 * mouseMoveDelta), Space.World);
    }

    public void MouseRightHandler(InputAction.CallbackContext context)
    {
        float mouseMoveDelta = context.valueSizeInBytes * sensivityHorizontal * Time.deltaTime;
        Vector3 res2 = Vector3.down * mouseMoveDelta;
        playerBody.Rotate(new Vector3(0, 1 * mouseMoveDelta), Space.World);
    }

    public void MouseDownHandler(InputAction.CallbackContext context)
    {
        float mouseMoveDelta = context.valueSizeInBytes * sensivityVertical * Time.deltaTime;
        Vector3 res2 = Vector3.right * mouseMoveDelta;
        math.clamp(mouseMoveDelta, -90, 90);

        //Debug.Log(camera_.rotation.eulerAngles.x);
        //Debug.Log(camera_.rotation.x);
        if (camera_.rotation.eulerAngles.x + mouseMoveDelta > 60 && camera_.rotation.eulerAngles.x + mouseMoveDelta < 360 || camera_.rotation.eulerAngles.x + mouseMoveDelta > 300 && camera_.rotation.eulerAngles.x + mouseMoveDelta <= 360)
        {
            //camera_.Rotate(Vector3.left, -mouseMoveDelta);
            //camera_.rotation.y = math.clamp(rotation.y, -yRotationLimit, yRotationLimit);
        }
        camera_.Rotate(Vector3.left, -mouseMoveDelta);
    }

    public void MouseUpHandler(InputAction.CallbackContext context)
    {
        float mouseMoveDelta = context.valueSizeInBytes * sensivityVertical * Time.deltaTime;
        Vector3 res2 = Vector3.left * mouseMoveDelta;
        //Debug.Log(camera_.rotation.eulerAngles.x);
        //Debug.Log(camera_.rotation.x);
        if (camera_.rotation.eulerAngles.x + mouseMoveDelta < 90 && camera_.rotation.eulerAngles.x + mouseMoveDelta > -88 || camera_.rotation.eulerAngles.x <= 360 && camera_.rotation.eulerAngles.x + mouseMoveDelta > 320)
        {
            //camera_.Rotate(Vector3.left, mouseMoveDelta);
        }
        camera_.Rotate(Vector3.left, mouseMoveDelta);
    }


    public void HorizontalMove(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Canceled)
        {
            horizontalMovePerformed = false;
        }
        if (context.phase == InputActionPhase.Started)
        {
            horizontalMovePerformed = true;
        }

        _horizontalInput = context.ReadValue<Vector2>();
        horizontalVelocity = (playerBody.transform.right * _horizontalInput.x + playerBody.transform.forward * _horizontalInput.y) * horizontalSpeed;
        movementPhysyc.Move(horizontalVelocity);
    }
    

    public void UseLeftHand(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            playerHands.TryShootLeft(rifleScope);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            playerHands.ReleaseLeft();
        }
    }
    public void UseRightHand(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            playerHands.TryShootRight(rifleScope);
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            playerHands.ReleaseRight();
        }
    }


    public void DropPickUpLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            playerHands.PickUpOrDropLeft(rifleScope);
        }
    }
    public void DropPickUpRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            playerHands.PickUpOrDropRight(rifleScope);
        }
    }


    public void ResetTarget(InputAction.CallbackContext context)
    {
        target.ResetTarget();
    }


    public void LockUnlockMouse(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            if (Cursor.lockState != CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    public void LockUnlockMouse()
    {
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    #endregion

}