using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Elementual;

public class WaterBallBehaviour : MonoBehaviour
{

    #region const
    private const string targetTagConst = "Target"; // Constant string for the tag of enemy targets.
    #endregion

    #region work fields
    private bool timerIsRunning = true;
    private float timeRemeaning = 5f;
    private float damage = 5f;
    private ElementalStatus status = new ElementalStatus(Statuses.Wet);
    #endregion

    #region Unity methods
    private void Update()
    {
        if (timerIsRunning && timeRemeaning > 0)
        {
            timeRemeaning -= Time.deltaTime;
            if (timeRemeaning < 0)
            {
                timerIsRunning = false;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == targetTagConst)
        {
            collision.gameObject.GetComponent<EnemyController>().ReciveDamage(damage, new ElementalStatus(Statuses.Wet));
            Destroy(gameObject);
            //Debug.Log("Destroy");
        }
    }
    #endregion

    #region Public method can be called wrom HandWeapon
    public void SetDamage(float damage_, ElementalStatus status_)
    {
        damage = damage_;
        status = status_;
    }
    #endregion


}
