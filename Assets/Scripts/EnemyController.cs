using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Mathematics;
using Elementual;
using System;
using Unity.VisualScripting;

public class EnemyController : MonoBehaviour
{

    #region Serialized Fields
    [SerializeField] private GameObject dummy;
    [SerializeField][Space] private TextMeshProUGUI hpText;
    [SerializeField] private float hpMax = 1000f;
    [SerializeField][Tooltip("Fire particles that will burn on the dummy")] private List<ParticleSystem> particlesFire = new List<ParticleSystem>();
    #endregion

    #region consts
    private const string particleTagConst = "Particle";
    private const float wetColorH = 238;
    private const float wetColorS = 36;
    private const float wetColorV = 64;
    private const float fireColorH = 0;
    private const float fireColorS = 20;
    private const float fireColorV = 90;
    #endregion

    #region work fields
    private float hp; // ResetTarget set hp = hpMax
    private bool startFire = false;
    private bool painted = false;
    private ElementalStatus currentStatus = new ElementalStatus();
    #endregion

    #region Unity methods
    private void Awake()
    {
        ResetTarget();
    }

    private void Update()
    {
        // When the fire status reaches 100, the dummy will burn until the status drops to 90
        if (currentStatus.statusPower == 100 && currentStatus.statusType == Statuses.Fire)
        {
            //Debug.Log("Max heat");
            currentStatus.statusPower -= 1 * Time.deltaTime;
            hp -= 5 * Time.deltaTime;
            startFire = true;
            StartFireParticles();
            SetHpText();
        }
        if (currentStatus.statusPower > 90 && startFire && currentStatus.statusType == Statuses.Fire)
        {
            currentStatus.statusPower -= 1 * Time.deltaTime;
            hp -= 5 * Time.deltaTime;
            SetHpText();
        }
        else if (currentStatus.statusPower <= 90 && startFire && currentStatus.statusType == Statuses.Fire)
        {
            //Debug.Log("Stop fire");
            startFire = false;
            StopFireParticles();
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == particleTagConst)
        {
            ReciveDamage(1f, new ElementalStatus(Statuses.Fire));
        }
    }
    #endregion

    #region Public methotds
    public void ReciveDamage(float damage, ElementalStatus elementalStatus)
    {
        if (hp > 0)
        {
            if (currentStatus.statusType == Statuses.Wet && elementalStatus.statusType == Statuses.Dry)
            {
                damage /= 2;
            }
            if (currentStatus.statusType == Statuses.Fire && elementalStatus.statusType == Statuses.Dry)
            {
                damage += 10;
            }
            hp -= damage;
        }
        currentStatus.AddStatus(elementalStatus);
        SetHpText();
    }

    public void ResetTarget()
    {
        dummy.SetActive(true);
        gameObject.SetActive(true);
        currentStatus = new ElementalStatus();
        hp = hpMax;
        SetHpText();
    }
    #endregion

    #region private methods
    private void DeactiveDummy()
    {
        dummy.SetActive(false);
        gameObject.SetActive(false);
    }

    private void SetHpText()
    {
        hpText.text = math.round(hp).ToString();
        if (currentStatus.statusType == Statuses.Fire)
        {
            hpText.color = new Color(math.clamp(currentStatus.statusPower / 100, 0.2f, 1f), 0.2f, 0.2f);
        }
        if (currentStatus.statusType == Statuses.Dry)
        {
            hpText.color = new Color(0.2f, 0.2f, 0.2f);
        }
        if (currentStatus.statusType == Statuses.Wet)
        {
            hpText.color = new Color(0.2f, 0.2f, math.clamp(currentStatus.statusPower / 100, 0.2f, 1f));
        }

        if (hp <= 0)
        {
            DeactiveDummy();
            StopFireParticles();
        }

        UpdateDummyMaterial();
    }

    private void StartFireParticles()
    {
        foreach (ParticleSystem particle in particlesFire)
        {
            particle.Play(true);
        }
    }
    private void StopFireParticles()
    {
        foreach (ParticleSystem particle in particlesFire)
        {
            particle.Stop(true);
        }
    }

    
    private void UpdateDummyMaterial()
    {
        //GetComponent<Renderer>().material
        Renderer renderer = dummy.GetComponent<Renderer>();
        Material material = dummy.GetComponent<Renderer>().material;
        if (currentStatus.statusType == Statuses.Wet)
        {
            float H = wetColorH;
            float S = PowerToColor(wetColorS);
            float V = PowerToColor(wetColorV, true);

            SetMatColor(material, H, S, V);
            painted = true;
        }
        else if (painted && currentStatus.statusType != Statuses.Fire)
        {
            Color nc = new Color(1, 1, 1);
            material.SetColor("_Color", nc);
            painted = false;
        }
        if (currentStatus.statusType == Statuses.Fire)
        {
            float H = fireColorH;
            float S = PowerToColor(fireColorS);
            float V = PowerToColor(fireColorV, true);

            SetMatColor(material, H, S, V);
            painted = true;
        }
    }

    private double PowerToColor(double scolor)
    {
        double result = 0f;
        result = CalculateRelative(0, 100, currentStatus.statusPower, 0, scolor);
        //Debug.Log("res " + result);
        return result;
    }
    private float PowerToColor(float scolor)
    {
        float result = 0f;
        result = CalculateRelative(0, 100, currentStatus.statusPower, 0, scolor);
        //Debug.Log("res " + result);
        return result;
    }
    private float PowerToColor(float scolor, bool reversed)
    {
        float result = 0f;
        if (reversed)
        {
            result = CalculateRelative(0, 100, currentStatus.statusPower, 100, scolor);
        }
        else
        {
            result = CalculateRelative(0, 100, currentStatus.statusPower, 0, scolor);
        }
        
        //Debug.Log("res " + result);
        return result;
    }

    private void SetMatColor(Material material, float H, float S, float V)
    {
        double h = CalculateRelative(0d, 360, H, 0, 1);
        double s = CalculateRelative(0d, 100, S, 0, 1);
        double v = CalculateRelative(0d, 100, V, 0, 1);
        float hh = Convert.ToSingle(h);
        float ss = Convert.ToSingle(s);
        float vv = Convert.ToSingle(v);
        Color nc = Color.HSVToRGB(hh, ss, vv);
        material.SetColor("_Color", nc);
    }

    private static float CalculateRelative(float range01, float range02, float x, float range11, float range12)
    {
        float relativePosition = (x - range01) / (float)(range02 - range01);

        float result = range11 + relativePosition * (range12 - range11);

        return result;
    }
    private static double CalculateRelative(double range01, double range02, double x, double range11, double range12)
    {
        double relativePosition = (x - range01) / (float)(range02 - range01);

        double result = range11 + relativePosition * (range12 - range11);

        return result;
    }

    
    #endregion

}
