using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Elementual
{
    public enum Statuses
    {
        Fire,
        Dry,
        Wet
    }

    public class ElementalStatus
    {
        public Statuses statusType;
        public float statusPower
        {
            get
            {
                return _statusPower;
            }

            set
            {
                _statusPower = math.clamp(value, -100, 100);
            }
        }
        private float _statusPower;

        public ElementalStatus()
        {
            statusType = Statuses.Dry;
            statusPower = 0;
        }
        public ElementalStatus(Statuses statusType_)
        {
            statusType = statusType_;
            statusPower = 0;
            if (statusType_ == Statuses.Fire)
            {
                statusPower = 1f;
            }
            if (statusType_ == Statuses.Wet)
            {
                statusPower = 10f;
            }
        }
        public ElementalStatus(Statuses statusType_, float statusPower_)
        {
            statusType = statusType_;
            statusPower = statusPower_;
        }

        public void AddStatus(ElementalStatus elementalStatus)
        {
            if (elementalStatus.statusType == Statuses.Fire && statusType == Statuses.Wet)
            {
                statusPower -= elementalStatus.statusPower;
                if (statusPower == 0)
                {
                    statusType = Statuses.Dry;
                }
                if (_statusPower < 0)
                {
                    statusType = elementalStatus.statusType;
                    statusPower = math.abs(statusPower);
                }
            }
            if (elementalStatus.statusType == Statuses.Wet && statusType == Statuses.Fire)
            {
                statusPower -= elementalStatus.statusPower;
                if (statusPower == 0)
                {
                    statusType = Statuses.Dry;
                }
                if (_statusPower < 0)
                {
                    statusType = elementalStatus.statusType;
                    statusPower = math.abs(statusPower);
                }
            }
            if (statusType == Statuses.Dry)
            {
                statusType = elementalStatus.statusType;
                statusPower = elementalStatus.statusPower;
            }

            if (statusType == elementalStatus.statusType)
            {
                statusPower += elementalStatus.statusPower;
            }
        }
    }

    public interface IHandWeapon
    {
        ElementalStatus status { get; }
        float damage { get; }
        public void PullTheTrigger(GameObject rifleScope);
        public void ReleaseTheTrigger();
        public void SetHand();
        public void SetWorld();
        public void SetHandRotation();
    }
}