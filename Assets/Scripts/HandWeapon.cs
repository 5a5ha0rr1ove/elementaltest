using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Elementual;

public class HandWeapon : MonoBehaviour, IHandWeapon
{

    #region Serialized Fields
    [SerializeField] private float damage_ = 10f;
    [SerializeField] private float handScale = 1f;
    [SerializeField] private float worldScale = 2f;
    [SerializeField][Space] private Statuses status_;
    [SerializeField] private ParticleSystem particlesToShoot;
    [SerializeField] private GameObject waterBallPrefab;
    [SerializeField][Tooltip("Rotate gameObject to 90 degrees when held")] private bool needRotate;
    #endregion

    #region Properties, implenetation of interface
    public ElementalStatus status { get; set; }
    public float damage { get {  return damage_; } }
    #endregion

    #region Consts
    private const string targetTagConst = "Target"; // Constant string for the tag of enemy targets.
    private const float ballSpawnDistance = 1f;
    private const float ballSpawnImpulseForce = 10f;
    private Vector3 ballScale = new Vector3(0.4f, 0.4f, 0.4f);
    #endregion

    #region work variables
    private bool readyToShoot = true;
    #endregion

    #region Unity methods
    private void Start()
    {
        status = new ElementalStatus(status_);
    }

    /// <summary>
    /// Called from the animator when the animation ends
    /// </summary>
    public void ReleaseTheTriggerAnim()
    {
        readyToShoot = true;
    }
    #endregion

    #region Implement IHandWeapon methods
    public void PullTheTrigger(GameObject rifleScope)
    {
        if (readyToShoot)
        {
            if (status.statusType == Statuses.Dry)
            {
                Animator animator = gameObject.GetComponent<Animator>();
                if (animator != null)
                {
                    animator.SetTrigger("PullTrigger");
                    readyToShoot = false;
                }
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(rifleScope.transform.position);
                if (Physics.Raycast(ray, out hit, 100))
                {
                    //Debug.Log(hit.collider.gameObject.tag); // Log the tag of the object hit by the ray.

                    // If the object hit has the "Target" tag, call the "ReciveDamage" function on its EnemyController component.
                    if (hit.collider.gameObject.tag == targetTagConst)
                    {
                        EnemyController enemyController = hit.collider.gameObject.GetComponent<EnemyController>();
                        enemyController.ReciveDamage(damage, status);
                    }
                }
            }
            if (status.statusType == Statuses.Fire)
            {
                particlesToShoot.Play(true);
            }
            if (status.statusType == Statuses.Wet)
            {
                // Calculate the spawn position one meter in front of the parent object.
                Vector3 spawnPosition = transform.position + transform.forward * ballSpawnDistance;
                // Spawn the prefab at the calculated position with the same rotation as the parent.
                GameObject spawnedPrefab = Instantiate(waterBallPrefab, spawnPosition, transform.rotation);
                spawnedPrefab.transform.localScale = ballScale;
                spawnedPrefab.GetComponent<WaterBallBehaviour>().SetDamage(damage, status);
                // Apply forward acceleration to the spawned prefab.
                Rigidbody spawnedRigidbody = spawnedPrefab.GetComponent<Rigidbody>();
                if (spawnedRigidbody)
                {
                    spawnedRigidbody.AddForce(transform.forward * ballSpawnImpulseForce, ForceMode.Impulse);
                }
                else
                {
                    Debug.LogWarning("The spawned prefab does not have a Rigidbody. Add a Rigidbody component to the prefab to apply acceleration.");
                }
            }
        }
        
    }
    public void ReleaseTheTrigger()
    {
        if (status.statusType == Statuses.Fire)
        {
            particlesToShoot.Stop();
        }
    }

    public void SetHand()
    {
        gameObject.transform.localScale = new Vector3(handScale, handScale, handScale);
        Animator animator = gameObject.GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("RotationBool", false);
        }
    }
    public void SetWorld()
    {
        gameObject.transform.localScale = new Vector3(worldScale, worldScale, worldScale);
        Animator animator = gameObject.GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("RotationBool", true);
        }
    }
    public void SetHandRotation()
    {
        if (needRotate)
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }
        else
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }
    #endregion

    
}
