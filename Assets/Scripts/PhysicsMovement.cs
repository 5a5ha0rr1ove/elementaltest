using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsMovement : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private SurfaceSlider slider;
    [SerializeField] private float speed;
    #endregion

    #region Public method can be called from InputHandler
    public void Move(Vector3 direction)
    {
        Vector3 directionAlongSurface = slider.Project(direction.normalized);
        Vector3 offset = directionAlongSurface * (speed * Time.deltaTime);

        rigidbody.MovePosition(rigidbody.position + offset);
    }
    #endregion

}
